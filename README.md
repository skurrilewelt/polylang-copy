# README #

![535108-user_256x256.png](http://wordpress.skurrilewelt.de/get_id/549)

Wordpress Plugin to enhance Polylang ().

Translate posts and pages automatically to all available languages or 
adds a dropdown to the row actions for post, pages and custom post types to copy items to another language with just one click. Copies not only the content but alos featured images, categories, tags and so on.

### How do I get set up? ###

* Download ZIP (prebuild: https://bitbucket.org/skurrilewelt/polylang-copy/downloads/polylang-copy-1.1.0.zip)
* Copy to plugins directory, unzip and activate
* OR: Clone into your plugins directory and activate
* Goto Settings / Polylang Copy Options and decide which method you prefer.

### Contribution ###

* Any contribution or suggestion is welcome

### Author and License ###

* GPL - Absolutely without any warranty
* Olaf Sweekhorst
* http://www.skurrilewelt.de
* info@skurrilewelt.de