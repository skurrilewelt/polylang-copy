<?php

/**
 *
 * @created 01.07.2017
 * @author: Olaf Sweekhorst <info@skurrilewelt.de>
 * @since 1.1.0
 */
class PolylangCopyOptions
{

    private $options;

    public function __construct()
    {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array( $this, 'page_init' ) );

    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Polylang Copy Options',
            'Polylang Copy Options',
            'manage_options',
            'polylang_copy_options',
            array($this, 'create_admin_page')
        );
    }


    public function create_admin_page()
    {

        wp_enqueue_style('slynx_client_css', POLYLANG_COPY_URL . '/polylang_copy_client.css');
        $this->options = get_option( 'polylang_copy_options' );

        if($this->options == false) {
            $this->options = [
                'autotranslate' => 0,
            ];
        }


        ?>
        <div class="wrap">
            <h1><?php _e('Polylang Copy Options', 'polylang-copy'); ?></h1>
            <form method="post" action="options.php" id="polylang_copy_options">
            <?php
                settings_fields( 'polylang_copy_options' );
                do_settings_sections('polylang_copy_options' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }


    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'polylang_copy_options', // Option group
            'polylang_copy_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'autotranslate', // ID
            __('Basic Settings', 'polylang-copy'), // Title
            array( $this, 'print_section_info' ), // Callback
            'polylang_copy_options' // Page
        );

        add_settings_field(
	        'auth_key', // ID
	        __('Autotranslate', 'slynx_client'), // Title
	        array( $this, 'auto_callback' ), // Callback
	        'polylang_copy_options', // Page
	        'autotranslate' // Section
        );
    }

    public function auto_callback()
    {
        $autotranslate = (key_exists('autotranslate', $this->options) && $this->options['autotranslate'] == '1') ? 'checked="checked"' : '';
        echo '
        <input type="checkbox" name="polylang_copy_options[autotranslate]" id="autotranslate" value="1" ' . $autotranslate . '>
		<label for="autotranslate">' . __('Automatically translate new posts and pages', 'polylang-copy') . '</label><br><br>';

    }

    /**
     * Print the Section text
     */
    public function print_section_info(){
    	$message = __('<p>If you check this option Polylang Copy will translate newly created posts automatically in all available languages.<br>', 'polylang-copy');
    	$message .= __('Leave this option unchecked, if you wan\'t to translate posts manually from your post or page list.</p>', 'polylang-copy');
    	print $message;
    }

}