<?php

/*
 Plugin Name: Polylang Copy
 Plugin URI: https://bitbucket.org/skurrilewelt/polylang-copy
 Description: Simple tool to copy posts and pages to any other language (needs polylang to be installed and activated)
 Author: Olaf Sweekhorst
 Version: 1.1.1
 Text Domain: polylang-copy
 Author URI: http://skurrilewelt.de
 */

class PolylangCopy
{

    public function __construct()
    {
        add_action('admin_init', array($this, 'init'), 10, 0);
        add_action('admin_enqueue_scripts', function () {
            wp_enqueue_script('poly-lang-js', plugins_url('polylang-copy.js', __FILE__), array('jquery'));
        });

    }

    public function init()
    {
        // Check if polylang exists and is actice
        if (!(is_plugin_active('polylang/polylang.php'))) {
            return;
        }
        if (is_admin()) {
            define('POLYLANG_COPY_DIR', dirname(__FILE__));
            define('POLYLANG_COPY_URL', plugins_url('polylang-copy'));
            require_once 'polylang-copy-options.php';
            new PolylangCopyOptions();
        }

        $options = get_option('polylang_copy_options');
        $autotranslate = false;
        if(is_array($options)) {
            $autotranslate = (key_exists('autotranslate', $options) && $options['autotranslate'] == '1') ? true : false;
        }

        // After selecting a new translation language we will come here again with some query params set:
        if (isset($_GET['polylang_copy_nonce']) && !wp_verify_nonce($_GET['polylang_copy_nonce'], basename(__FILE__))) {
            if (!in_array($_GET['translation'], pll_languages_list())) {
                return;
            }
            $this->copy_post((int)$_GET['post_id'], $_GET['translation']);
        } else {


            if($autotranslate === true) {
                add_action('save_post', array($this, 'autoTranslate'), 10, 3);
            }

        }

        if($autotranslate === false) {
            add_filter('page_row_actions', array($this, 'row_actions_callback'), 10, 2);
            add_filter('post_row_actions', array($this, 'row_actions_callback'), 10, 2);
        }

        load_textdomain('polylang-copy', __DIR__ . '/languages/polylang-copy-de_DE.mo');

    }

    /**
     * This functions triggers whenever a post is saved. In our case we must wait
     * until a post has content and check afterwards for translated post versions.
     * @param int $post_id
     * @param WP_Post $post
     * @param bool $update
     */
    public function autoTranslate($post_id, $post, $update)
    {
        if ($update == false ||
            wp_is_post_revision($post_id) ||
            'trash' == get_post_status($post_id) ||
            'inherit' == get_post_status($post_id)
        ) {
            return;
        }

        // remove hook to avoid infinte loops
        remove_action('save_post', array($this, 'autoTranslate'));

        $translated_posts = pll_get_post_translations($post_id);
        $available_languages = pll_languages_list();
        // Create translations only if needed and skip existing translations
        $to_translate_languages = array_diff($available_languages, array_keys($translated_posts));

        $done = [];

        foreach ($to_translate_languages as $to_translate_language) {
            $done[] = $this->copy_post($post_id, $to_translate_language, false);
        }

        $pll_translate_array = [];
        $pll_translate_array[pll_get_post_language($post_id)] = $post->ID;

        foreach ($done as $translated_post) {
            $this->set_terms($post_id, $translated_post->ID);
            $this->copy_post_meta($post_id, $translated_post->ID);
            $pll_translate_array[pll_get_post_language($translated_post->ID)] = $translated_post->ID;
        }

        // Add the translations to the term_taxonomy 'post_translations'
        if (count($pll_translate_array) > 1) {
            pll_save_post_translations($pll_translate_array);
        }

        add_action('save_post', array($this, 'autoTranslate'), 10, 3);
        wp_redirect(admin_url('post.php?action=edit&post=' . $post_id));
        exit;
    }


    /**
     * Adds the dropdown with available translation languages to the
     * row actions
     * @param $actions
     * @param $post
     * @return mixed
     */
    public function row_actions_callback($actions, $post)
    {
        $nonce = wp_create_nonce('polylang_copy_nonce');
        $link = admin_url("edit.php?post_id={$post->ID}&polylang_copy_nonce=$nonce") . '&translation=';
        $languages = pll_languages_list();
        $language_labels = pll_languages_list(['fields' => 'name']);
        $languages = array_combine($languages, $language_labels);
        //remove the current post language
        unset($languages[pll_get_post_language($post->ID)]);

        $html = '<select data-href="' . $link . '" class="polylang_copy_select">';
        $html .= '<option value="">' . __('Copy to language', 'polylang-copy') . '</option>';
        foreach ($languages as $option_value => $option_label) {
            $html .= '<option value="' . $option_value . '">' . $option_label . '</option>';
        }
        $actions['polylang_copy'] = '<br>' . $html;
        return $actions;
    }

    /**
     * The main method to copy a post or page.
     * New: Redirect defaults to true for calling from the post list view.
     * Automatically translating may not redirect and exit.
     * @param int $post_id
     * @param string $target_language
     * @param bool $redirect
     */
    public function copy_post($post_id, $target_language, $redirect = true)
    {
        $post = get_post($post_id);
        $current_user = wp_get_current_user();
        $post_author = $current_user->ID;

        $new_post = array(
            'comment_status' => $post->comment_status,
            'ping_status' => $post->ping_status,
            'post_author' => $post_author,
            'post_content' => $post->post_content,
            'post_excerpt' => $post->post_excerpt,
            'post_name' => $post->post_name,
            'post_parent' => $post->post_parent,
            'post_password' => $post->post_password,
            'post_status' => 'publish', //$post->post_status,
            'post_title' => $post->post_title . ' - ' . $target_language,
            'post_type' => $post->post_type,
            'to_ping' => $post->to_ping,
            'menu_order' => $post->menu_order
        );

        $insert_id = wp_insert_post($new_post);
        pll_set_post_language($insert_id, $target_language);
        // Save translation map only in single action mode
        if ($redirect === true) {

            //Take in account thta there are maybe other existing translations:
            $translated_posts = pll_get_post_translations($post_id);
            $translated_posts[$target_language] = $insert_id;
            /*
            pll_save_post_translations([
                pll_get_post_language($post_id) => $post->ID,
                $target_language => $insert_id
            ]);
            */

            pll_save_post_translations($translated_posts);

            $this->set_terms($post_id, $insert_id);
            $this->copy_post_meta($post_id, $insert_id);
        }

        if ($redirect === true) {
            wp_redirect(admin_url('post.php?action=edit&post=' . $insert_id));
            exit;
        } else {
            return get_post($insert_id);
        }
    }

    private function set_terms($source_post_id, $target_post_id)
    {
        $source_post = get_post($source_post_id);
        $target_language = pll_get_post_language($target_post_id);
        $taxonomies = get_object_taxonomies($source_post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
        $new_term_ids = [];
        foreach ($taxonomies as $taxonomy) {
            // post translation and language is already set
            if ($taxonomy == 'post_translations' || $taxonomy == 'language') {
                continue;
            }
            $post_terms = wp_get_object_terms($source_post_id, $taxonomy);
            foreach ($post_terms as $post_term) {
                //If there is no translation, pll will give us false, then save the original term to the new post
                if (($translated_term_id = pll_get_term($post_term->term_id, $target_language)) !== false) {
                    $new_term_ids[] = $translated_term_id;
                } else {
                    $new_term_ids[] = $post_term->term_id;
                }
            }
            wp_set_object_terms($target_post_id, $new_term_ids, $taxonomy, false);
        }
    }

    private function copy_post_meta($post_id, $new_post_id)
    {
        global $wpdb;
        $sql = 'SELECT 
                    `meta_key`, `meta_value`
                FROM ' .
            $wpdb->postmeta . '
                WHERE
                    `post_id` = ' . (int)$post_id;
        $post_meta_infos = $wpdb->get_results($sql);
        if (count($post_meta_infos) != 0) {
            $sql_query = 'INSERT INTO ' . $wpdb->postmeta . '(post_id, meta_key, meta_value) ';
            foreach ($post_meta_infos as $meta_info) {
                $meta_key = $meta_info->meta_key;
                $meta_value = addslashes($meta_info->meta_value);
                $sql_query_sel[] = "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }
            $sql_query .= implode(" UNION ALL ", $sql_query_sel);
            $wpdb->query($sql_query);
        }
    }

}

$polylang_copy = new PolylangCopy();